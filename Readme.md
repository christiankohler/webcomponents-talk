## Run presentation
- npm install
- bower install
- gulp (if necessary: npm install -g gulp)

## Demo

1. Ziel zeigen  // curtis
-------------------------------------------- custom elements
2. script tag
3. register     // regel
4. proto        // varbadge
5. callback     // createdclb
-------------------------------------------- templates
6. template     // tmplbadge
7. elem         // tmplel
9. content      // tmplcont
10.appendChild
11.copy all
--------------------------------------------- shadow dom
12.createShadowRoot()
13.copy css
14.class to id
--------------------------------------------- HTML Import
15.move to html
16.importDoc    // ownerdoc
--------------------------------------------- data binding
17.name dynamic // getfullname getphoto
18.second badge // courtney
-------------------------------------------- public methods
19.setName      // setname
20.johnny
21.attribute    // changedclb
