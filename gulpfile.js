var gulp      = require('gulp');
var webserver = require('gulp-webserver');
var concat    = require('gulp-concat');
var bump      = require('gulp-bump');
var include   = require('gulp-file-include');

var paths = {
  template : "layout/index.html",
  presentation : "presentation/",
  sourcefiles : ['./slides/**/*.html', '!./slides/allslides.html']
};

gulp.task('webserver', function() {
  gulp.src('presentation')
    .pipe(webserver({
      livereload: false,
      open: true,
      port: 8001
    }));
});

gulp.task("build", ["concat"],  function() {
    gulp.src(paths.template)
        .pipe( include() )
        .pipe( gulp.dest(paths.presentation) );
});

gulp.task('concat', function() {
  return gulp.src(paths.sourcefiles)
    .pipe(concat('allslides.html'))
    .pipe(gulp.dest('./slides'));
});

gulp.task('watch', function(){
  gulp.watch(paths.sourcefiles, ['build']);
});

gulp.task('minor', function(){
  gulp.src('./bower.json')
  .pipe(bump({type:'minor'}))
  .pipe(gulp.dest('./'));
});

gulp.task('default', ['build', 'watch', 'webserver']);
